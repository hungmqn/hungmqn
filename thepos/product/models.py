import uuid
from django.db import models
from decimal import Decimal
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django.db import models
from django.template.defaultfilters import slugify
from django.utils import timezone
from django.core.validators import MinValueValidator, MaxValueValidator
# Create your models here.

class Product(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
        unique=True
    )
    name = models.CharField(max_length=256, verbose_name=_('Name'),
                            help_text=_('Enter name of the product...'), null=True, blank=False)
    image = models.ImageField(
        upload_to='products/', height_field=None, width_field=None, max_length=None)
    description = models.CharField(max_length=1000, verbose_name=_('Description'),
                                   help_text=_('Enter description...'))
    unit_price = models.DecimalField(max_digits=6, decimal_places=0, validators=[MinValueValidator(
        Decimal(1000)), MaxValueValidator(Decimal(999999))], null=True, blank=False)
    created_at = models.DateTimeField(
        auto_now_add=True, null=True, editable=False)
    modified_at = models.DateTimeField(auto_now=True, blank=False)

    def __str__(self):
        return self.name

    def get_name_slug(self):
        return slugify(self.name)

    class Meta:
        # db_table = ''
        # managed = True
        verbose_name = 'menu'
        verbose_name_plural = 'menu'
        ordering = ['modified_at']