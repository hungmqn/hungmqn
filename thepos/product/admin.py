from django.contrib import admin

# Register your models here.

from django.utils.translation import gettext_lazy as _
from django.utils.safestring import mark_safe
from django.conf import settings
# Register your models here.
from .models import Product

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('preview_image', 'name',
                    'formatted_unit_price', 'last_modified')

    readonly_fields = ['preview_image']

    def formatted_unit_price(self, obj):
        return '{:,.0f} VND'.format(obj.unit_price)
    formatted_unit_price.short_description = 'price'

    def preview_image(self, obj):
        if obj.image:
            return mark_safe('<img src="{url}" style="object-fit: contain;" width="100" height="100" />'.format(
                url=obj.image.url,
                # width=obj.image.width,
                # height=obj.image.height,
            ))
        else:
            return mark_safe('<img src="{url}" style="object-fit: contain;" width="100" height="100" />'.format(
                url='/mediafiles/dishes/no-image.png',
                # width=100,
                # height=100,
            ))

    def last_modified(self, obj):
        return obj.modified_at
