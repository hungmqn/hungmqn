# Setting up

1. Set up SSL

## Local

```
openssl req -x509 -out localhost.crt -keyout localhost.key \
  -newkey rsa:2048 -nodes -sha256 \
  -subj '/CN=localhost' -extensions EXT -config <( \
   printf "[dn]\nCN=localhost\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:localhost\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")

```

Copy generated ssl to

## Production

### Option 1: In real machine with Let's encrypt:

```
sudo add-apt-repository ppa:certbot/certbot
sudo apt install python-certbot-nginx
```

Change example.com with the domain
Nginx:

```
sudo certbot --nginx -d example.com -d www.example.com
```

Apache

```
sudo certbot --apache -d example.com -d www.example.com
```

### Option 2: Docker configuration

## Test

1. This configuration is to set up auto renewal ssl using certbot image.

```
chmod +x test-init-letsencrypt.sh
```

2. Configure

Update docker-compose.prod.yml for ssl certificate folder on production.

Update virtualhost.conf in docker/nginx/config/prod/ for ssl files

3. Init

```
sudo bash test-init-letsencrypt.sh
```

## Do it for real

1. This configuration is to set up auto renewal ssl using certbot image.

```
chmod +x init-letsencrypt.sh
```

2. Configure

Update docker-compose.prod.yml for ssl certificate folder on production.

Update virtualhost.conf in docker/nginx/config/prod/ for ssl files

3. Init

```
sudo bash init-letsencrypt.sh
```

# Commands

## Local

1. Docker command

Start

```
docker-compose up -d
```

Stop

```
docker-compose stop
```

Down

```
docker-compose down
```

2. Database backup and restore

Set variables

```
DBUSER="sawp" DB="dbwp" DBPWD="dbpwd" CONTAINERNAME="hungmqn_db_1" DBBUDATE="$(date +'%F-%H%M%S')"
```

Backup

```
// Linux shell
sudo docker exec $CONTAINERNAME /usr/bin/mysqldump -u $DBUSER --password=$DBPWD $DB > backup/local/backup-$DBBUDATE.sql
// Windows
docker exec hungmqn_db_1 /usr/bin/mysqldump -u sawp --password=dbpwd dbwp > backup/local/backup.sql
```

Restore

```
// Linux shell
sudo cat <sql_file_path> | docker exec -i $CONTAINERNAME /usr/bin/mysql -u $DBUSER --password=$DBPWD --default-character-set=utf8mb4 $DB
// Windows
cat <sql_file_path> | docker exec -i hungmqn_db_1 /usr/bin/mysql -u sawp --password=dbpwd --default-character-set=utf8 dbwp
```

## Production

1. Docker command

Start

```
sudo docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d
sudo docker-compose -f docker-compose.yml -f docker-compose.prod.ssl.yml up -d
sudo docker-compose -f docker-compose.prod.ssl.yml up -d
```

Stop

```
sudo docker-compose stop
```

Down

```
sudo docker-compose down
```

2. Database backup and restore

Set variables

```
DBUSER="sawp" DB="dbwp" DBPWD="dbpwd" CONTAINERNAME="hungmqn_db_1" DBBUDATE="$(date +'%F-%H%M%S')"

```

Backup

```
sudo docker exec $CONTAINERNAME /usr/bin/mysqldump -u $DBUSER --password=$DBPWD $DB > backup/prod/backup-$DBBUDATE.sql
```

Restore

```
sudo cat backup/prod/<sql_file_name> | docker exec -i $CONTAINERNAME /usr/bin/mysql -u $DBUSER --password=$DBPWD $DB
```

3. Build frontend app

```
cd path_to_app
sudo docker build .
sudo docker run
```

# Useful commands

```
// Grant 755 permission for real machine
sudo chmod -R 755 wordpress
// Go to service
sudo docker-compose exec -u 0 service_name bash
// Go to wordpress container
sudo docker-compose exec -u 0 php bash
// List files and folders in wordpress container
sudo docker-compose exec -u 0 php ls -l
// Grant permission for working directory inside container
sudo docker-compose exec -u 0 php chown -R 1000 /the-dir
sudo docker-compose exec -u 0 php chmod 777 -R /the-dir
// Grant permission for specific directory
sudo chmod 777 -R folder

Download
File
scp username@ip/domain:file_from_host destination_on_local
Directory
scp -r username@ip/domain:dir_from_host destination_on_local
Upload
File
scp file_on_local username@ip/domain:destination_on_host
Directory
scp -r dir_on_local username@ip/domain:destination_on_host
```

### Useful links

`<apache>` https://www.digitalocean.com/community/tutorials/how-to-secure-apache-with-let-s-encrypt-on-ubuntu-18-04

`<nginx>` https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-18-04

`<documentation>` https://docs.google.com/document/d/1oGCt2spYinty8UYbVAc1WkxJZm4Frr8C0gpF5VLWXIg/edit?usp=sharing
